# This file is a template, and might need editing before it works on your project.
FROM python:3.6

ARG PAT_NAME
ARG PAT_VALUE

RUN pip install calculater --index-url https://${PAT_NAME}:${PAT_VALUE}@gitlab.com/api/v4/projects/47131217/packages/pypi/simple

ENV FLASK_APP=calculater

CMD ["flask", "run","--host","0.0.0"]
